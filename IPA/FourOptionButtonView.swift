//
//  TwoOptionButtonView.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Check us out @ https://shooting-unicorns.com/ 🖥 🎨 🦄
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class FourOptionButtonView: UIView {
    
    var firstOption: String? {
        didSet {
            firstButton.setTitle("\(firstOption!)", for: .normal)
        }
    }
    
    var secondOption: String? {
        didSet {
            secondButton.setTitle("\(secondOption!)", for: .normal)
        }
    }
    
    var thirdOption: String? {
        didSet {
            thirdButton.setTitle("\(thirdOption!)", for: .normal)
        }
    }

    var fourthOption: String? {
        didSet {
            fourthButton.setTitle("\(fourthOption!)", for: .normal)
        }
    }
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var fourthButton: UIButton!
    @IBOutlet weak var optionSelected: UILabel!
    var notificationType: NotificationType!
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
        optionSelected.isHidden = true
        firstButton.layer.cornerRadius = 11
        secondButton.layer.cornerRadius = 11
        thirdButton.layer.cornerRadius = 11
        fourthButton.layer.cornerRadius = 11
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "FourOptionButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        
    }
    
    @IBAction func didPressButton(_ sender: AnyObject) {
        optionSelected.isHidden = false
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
        fourthButton.isHidden = true
        optionSelected.layer.cornerRadius = 11
        optionSelected.text = sender.titleLabel??.text
        responseReceived(option: sender.titleLabel??.text ?? "")
    }
    
    func responseReceived(option: String ) {
        UserDefaults.setUserSelectedOption(option)
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(notificationType.rawValue), object: nil)
    }
    
    private func hideOptionButtons() {
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
        fourthButton.isHidden = true
    }
}

