//
//  Extensions.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Check us out @ https://shooting-unicorns.com/ 🖥 🎨 🦄
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import Foundation

enum Topic: Int {
    case introduction
    case application
    
    static let allValues = [introduction, application]
}

enum Status: String {
    case pending
    case notSent
    case sent
}

extension UserDefaults {
    
    static let defaults = UserDefaults.standard
    
    static func setUserSelectedOption(_ option: String) {
        defaults.set(option, forKey: "lastSelectedOption")
    }
    
    static func getUserSelectedOption() -> String? {
        if let lastOption = defaults.object(forKey: "lastSelectedOption") as? String {
            return lastOption
        } else {
            return nil
        }
    }
}

enum NotificationType: String {
    case onboarding = "onboarding"
    case checkIfApplicationExists = "checkIfApplicationExists"
    case processUserQuery = "processUserQuery"
    case viewOrFavourite = "viewOrFavourite"
}

extension String {
    var isNumeric: Bool {
        guard self.characters.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self.characters).isSubset(of: nums)
    }
}
