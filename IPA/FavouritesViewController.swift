//
//  FavouritesViewController.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import UIKit

class FavouritesViewController: UIViewController, IPReturned {
    var patent: Patent? {
        didSet{
            if let patent = patent {
                DispatchQueue.main.async {
                    self.dataReturned.append(patent)
                    self.favouriteTableView.reloadData()
                }
            }
        }
    }
    
    let statusClient = StatusClient()
    var tradeMark: TradeMark? {
        didSet{
            if let tradeMark = tradeMark {
                DispatchQueue.main.async {
                    self.dataReturned.append(tradeMark)
                    self.favouriteTableView.reloadData()
                }
            }
        }
    }
    var favourites: [IP] = [IP]()
    var message: String?
    var dataReturned = [IP]()
    var temp = [(stage: String, index: Int)]()
    var stageCount = 0
    var rowDisplayArray = [[NSDictionary]]()
    
    @IBOutlet weak var favouriteTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusClient.delegate = self
        //remove later
        statusClient.fetchStatusForTradeMark("1")
        
        favourites.forEach({ (ip) in
            if let tradeMark = ip as? TradeMark {
                print("fetching tradeMarK")
                statusClient.fetchStatusForTradeMark(tradeMark.applicationId!)
            }
        })
    }
}

extension FavouritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FavouriteCell
        let cellData = rowDisplayArray[indexPath.section][indexPath.row]
        
        if(cellData.object(forKey: "Status") != nil) {
            cell.title.text = cellData.object(forKey: "Status") as? String ?? ""
            cell.title.font = UIFont.boldSystemFont(ofSize: 14)
            cell.statusIcon.setImage(#imageLiteral(resourceName: "current-icon"), for: .normal)
        } else if(cellData.object(forKey: "Date") != nil) {
            var dateString = ""
            let date = cellData.object(forKey: "Date") as? NSDictionary
            for (dateType, actualDate) in date! {
                let dateType = dateType as? String ?? ""
                let actualDate = actualDate as? String ?? ""
                dateString = dateType + ": " + actualDate
            }
            
            cell.title.text = dateString
            cell.title.font.withSize(13)
            cell.statusIcon.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let data = dataReturned[section] as? TradeMark {
            return "Application ID: \(data.applicationId!)"
        }
        return "N/A"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataReturned.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let data = dataReturned[section] as? TradeMark {
            // Add number of stages into count
            var itemArray = [NSDictionary]()
            data.stages?.forEach({ (stageItem) in
                count += 1
                count += stageItem.dates.count
                itemArray.append(["Status": stageItem.stage ?? ""])
                stageItem.dates.forEach({ (date) in
                    itemArray.append(["Date": date])
                })
            })
            
            self.rowDisplayArray.append(itemArray)
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

class FavouriteCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var statusIcon: UIButton!
    @IBOutlet weak var timelineBarView: UIView!
    
    override func prepareForReuse() {
        title.text = nil
        title.font = UIFont.systemFont(ofSize: 14)
        statusIcon.setImage(#imageLiteral(resourceName: "current-icon"), for: .normal)
    }
}
