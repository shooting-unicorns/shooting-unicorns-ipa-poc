//
//  StatusClient.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import Foundation

protocol IPReturned {
    var tradeMark: TradeMark? { get set }
    var patent: Patent? { get set }
    var favourites: [IP] { get set }
    var message: String? { get set }
}

protocol IP {}
//
//class TradeMark: IP {
//    var stages: [(stage: String, dates: NSArray)]?
//    var timelineStatus: String?
//    var applicationId: String?
//}
class TradeMark: IP {
    var stages: [StageModel]?
    var timelineStatus: String?
    var applicationId: String?
}

class StageModel {
    var stage: String?
    var completed: Bool?
    var dates = [[String: String]] ()
}

class Patent: IP {
    var status: String?
    var averageDuration: Int?
}

class StatusClient {
    let TRADEMARK_URL = "https://search.ipaustralia.gov.au/trademarks/search/view/{id}/status"
    let PATENT_URL = "http://13.55.19.128:3000/getPatentById/{id}"
    var delegate: IPReturned?
    let session = URLSession.shared
    
    func fetchStatusForPatent(_ id: String) {
        let url = PATENT_URL.replacingOccurrences(of: "{id}", with: id)
        let request = URLRequest(url: URL(string: url)!)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard error == nil else { return }
            do {
                let resultJson = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                if let result = resultJson?["result"] as? NSDictionary {
                    if let currentStatus = result["currentStatus"] as? NSDictionary {
                        let patent = Patent()
                        if let status = currentStatus["status"] as? String {
                            patent.status = status
                        }
                        if let averageDuration = currentStatus["averageDuration"] as? Int {
                            patent.averageDuration = averageDuration
                        }
                        DispatchQueue.main.async {
                            self.delegate?.patent = patent
                        }
                    }
                }
            } catch {
                self.delegate?.message = "No matches found 😢"
            }
        })
        dataTask.resume()
    }

    func fetchStatusForTradeMark(_ id: String) {
        let url = TRADEMARK_URL.replacingOccurrences(of: "{id}", with: id)
        let request = URLRequest(url: URL(string: url)!)
        let tradeMark = TradeMark()
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard error == nil else { return }
            do {
                
                let resultJson = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                
                // application id
                tradeMark.applicationId = id
    
                // status
                if let timelineStatus = resultJson?["timelineStatus"] as? String {
                    tradeMark.timelineStatus = timelineStatus
                }
                // get stages
                if let stages = resultJson?["stages"] as? NSArray {
                    for stage in stages {
                        if let stage = stage as? NSDictionary {
                            
                            // 😒 👎
//                            var stageLabel: String?
//                            var dates: NSDictionary?
                            let stageItem = StageModel()
                            
                            if let innerStage = stage["stage"] as? NSDictionary {
//                                stageLabel = innerStage["label"] as? String
                                stageItem.stage = innerStage["label"] as? String
                            }
                            // get completed
                            if let completed = stage["stage"] as? NSDictionary {
                                stageItem.completed = completed["completed"] as? Bool
                            }
                            
                            // get dates
                            if let datesDic = stage["dates"] as? NSDictionary {
//                                dates = datesDic
//                                var dateObj = [String: String] ()
                                for(dateType, date) in datesDic {
                                    stageItem.dates.append([(dateType as? String)!: (date as? String)!])
                                }
                            }
//                            
//                            
                            if (tradeMark.stages != nil) {
                                tradeMark.stages?.append(stageItem)
                            } else {
                                tradeMark.stages = [stageItem]
                            }
                        }
                    }
                }
                self.delegate?.tradeMark = tradeMark
                
            } catch {
                self.fetchStatusForPatent(id)
            }
        })
        dataTask.resume()
    }
}
