//
//  TwoOptionButtonView.swift
//  IPA
//
//  Created by Samantha Wong on 30/7/17.
//  Check us out @ https://shooting-unicorns.com/ 🖥 🎨 🦄
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class StatusTimelineView: UIView {
    
    var notificationType: NotificationType!
    var status: String? {
        didSet{
            setupView()
        }
    }
    @IBOutlet weak var bar1: UIView!
    @IBOutlet weak var bar2: UIView!
    @IBOutlet weak var bar3: UIView!
    @IBOutlet weak var status1: UIButton!
    @IBOutlet weak var status2: UIButton!
    @IBOutlet weak var status3: UIButton!
    @IBOutlet weak var status4: UIButton!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    func setupView() {
        switch status!.lowercased() {
        case "filed":
            status1.setImage(#imageLiteral(resourceName: "current-icon"), for: .normal)
            status2.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
            status3.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
            status4.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
        case "examined":
            status1.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status2.setImage(#imageLiteral(resourceName: "current-icon"), for: .normal)
            status3.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
            status4.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
        case "acceptance / opposition":
            status1.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status2.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status3.setImage(#imageLiteral(resourceName: "current-icon"), for: .normal)
            status4.setImage(#imageLiteral(resourceName: "circle-icon-grey"), for: .normal)
        case "registered":
            status1.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status2.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status3.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
            status4.setImage(#imageLiteral(resourceName: "complete-icon"), for: .normal)
        default:
            break
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "StatusTimelineView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }

    func responseReceived(option: String ) {
        UserDefaults.setUserSelectedOption(option)
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(notificationType.rawValue), object: nil)
    }
}

