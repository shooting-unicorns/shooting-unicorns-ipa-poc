//
//  ViewController.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Check us out @ https://shooting-unicorns.com/ 🖥 🎨 🦄
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import UIKit
import NMessenger
import AudioToolbox
import AsyncDisplayKit
class ViewController: NMessengerViewController, IPReturned {
    var favourites: [IP] = [IP]()

    var count = 1.5
    let nc = NotificationCenter.default
    let statusClient = StatusClient()
    var lastIP: IP?
    var tradeMark: TradeMark? {
        didSet {
            lastIP = tradeMark
            sendResults()
        }
    }
    var patent: Patent? {
        didSet {
            lastIP = patent
            sendResults()
        }
    }
    var message: String? {
        didSet {
            if let message = message {
                sendMessage(message: message, incoming: true)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nc.addObserver(self, selector: #selector(ViewController.startCheckIfApplicationExists), name: Notification.Name(NotificationType.onboarding.rawValue), object: nil)
        
        nc.addObserver(self, selector: #selector(ViewController.processUserQuery), name: Notification.Name(NotificationType.processUserQuery.rawValue), object: nil)
        
        nc.addObserver(self, selector: #selector(ViewController.viewOrFavourite), name: Notification.Name(NotificationType.viewOrFavourite.rawValue), object: nil)
        
        inputBarView.isHidden = true
        statusClient.delegate = self
        startOnboarding()
    }

    // MARK: - Conversation methods
    
    func startOnboarding() {
        
        sendMessage(message: "Hello I'm IPsum!", incoming: true)
        sendMessage(message: "I'm here to message you the latest updates to your application(s) 👍", incoming: true)
        sendMessage(message: "Do you have an existing application?", incoming: true)
        sendTwoOptions(firstOption: "Yes", secondOption: "No", notification: .onboarding)
        
        count = 0
    }
    
    func startCheckIfApplicationExists() {
        if UserDefaults.getUserSelectedOption() == "Yes" {
            sendMessage(message: "Cool! You can type in your application number and start tracking the status 👌", incoming: true)
        } else {
            sendMessage(message: "We currently only support searches using application number. Please visit IP Australia if you're looking to browse using keywords 😔.", incoming: true)
        }
        
        self.inputBarView.isHidden = false
        count = 0
    }
    
    // MARK: - Search

    func processUserQuery(_ notification: Notification) {
        
        if let myDict = notification.object as? [String: Any] {
            if let userInput = myDict["processUserQuery"] as? String {
                if userInput.isNumeric {
                    statusClient.fetchStatusForTradeMark(userInput)
                } else {
                    sendMessage(message: "Please enter a valid application number ❌", incoming: true)
                }
            }
        }
    }
    
    func sendResults() {
        if let ip = lastIP as? Patent {
            if let status = ip.status {
               sendMessage(message: "The patent's status is currently in \(status) and is expected to complete in \(ip.averageDuration?.description ?? "N/A") months.", incoming: true)
            }
        }
        
        if let ip = lastIP as? TradeMark {
            // check status using completion
            if let stages = tradeMark?.stages {
                var foundIncompleteStage = false
                for stage in stages {
                    if let completed = stage.completed {
                        if !completed {
                            sendStatusTimeline(stage.stage ?? "")
                            sendTwoOptions(firstOption: "", secondOption: "Favourite ⭐️", notification: NotificationType.viewOrFavourite)
                            foundIncompleteStage = true
                            break
                        }
                    }
                }
                // check if completed where no completed stage exist
                if !foundIncompleteStage {
                    if (ip.timelineStatus?.lowercased().contains("registered"))! {
                        sendMessage(message: "Status is \(ip.timelineStatus ?? "N/A")", incoming: true)
                        sendTwoOptions(firstOption: "", secondOption: "Favourite ⭐️", notification: NotificationType.viewOrFavourite)
                    }
                }
            }
        }
    }
    
    func viewOrFavourite() {
        if UserDefaults.getUserSelectedOption() == "View" {
            print("user selected view")
        } else {
            if let lastIP = lastIP {
                favourites.append(lastIP)
                sendMessage(message: "Done! you can view your favourites by clicking on the favourites icon on the navigation bar", incoming: true)
            }
        }
    }
    
    // MARK: - Helper methods
    
    private func sendMessage(message: String, incoming: Bool) {
        self.count += 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + count, execute: {
            let message = self.sendText(message, isIncomingMessage:incoming)
            message.cellPadding = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 20)
            
            if incoming {
                AudioServicesPlaySystemSound(1003)
            } else {
                AudioServicesPlaySystemSound(1004)
            }
        })
    }

    private func sendTwoOptions(firstOption: String, secondOption: String, notification: NotificationType) {
        self.count += 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + count, execute: {
            let twoOptionButton = TwoOptionButtonView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
            
            twoOptionButton.firstOption = firstOption
            twoOptionButton.secondOption = secondOption
            twoOptionButton.notificationType = notification
            let customOption = self.createCustomContentViewMessage(twoOptionButton, isIncomingMessage: false)
            customOption.cellPadding = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 20)
            
            self.addMessageToMessenger(customOption)
        })
    }
    
    private func sendStatusTimeline(_ status: String) {
        self.count += 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + count, execute: {
          
            let statusTimeline = StatusTimelineView(frame: CGRect(x: 0, y: 0, width: 375, height: 50))
            statusTimeline.status = status
            
            let customOption = self.createCustomContentViewMessage(statusTimeline, isIncomingMessage: false)
            customOption.cellPadding = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 20)
            
            self.addMessageToMessenger(customOption)
        })
    }
    
    private func sendIPTypeOption() {
        self.count += 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + count, execute: {
            let fourOptionButton = FourOptionButtonView(frame: CGRect(x: 0, y: 0, width: 375, height: 50))
            
            fourOptionButton.firstOption = "Trademark"
            fourOptionButton.secondOption = "Patent"
            fourOptionButton.thirdOption = "Design"
            fourOptionButton.fourthOption = "Plant Breeder"
            
            let customOption = self.createCustomContentViewMessage(fourOptionButton, isIncomingMessage: false)
        
            customOption.cellPadding = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 20)
            
            
            self.addMessageToMessenger(customOption)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destVC = segue.destination as? FavouritesViewController {
            destVC.favourites = favourites
        }
    }
    
    @IBAction func didPressFavouritesButton(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "showFavourites", sender: self)
    }
}

