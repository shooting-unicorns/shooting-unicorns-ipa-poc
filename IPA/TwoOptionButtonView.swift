//
//  TwoOptionButtonView.swift
//  IPA
//
//  Created by Samantha Wong on 29/7/17.
//  Check us out @ https://shooting-unicorns.com/ 🖥 🎨 🦄
//  Copyright © 2017 Shooting Unicorns. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class TwoOptionButtonView: UIView {
    
    var firstOption: String? {
        didSet {
            if firstOption == "" {
                firstButton.isHidden = true
                return
            }
            firstButton.setTitle("\(firstOption!)", for: .normal)
        }
    }
    
    var secondOption: String? {
        didSet {
            secondButton.setTitle("\(secondOption!)", for: .normal)
        }
    }
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var optionSelected: UILabel!
    var notificationType: NotificationType!
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
        optionSelected.isHidden = true
        firstButton.layer.cornerRadius = 11
        secondButton.layer.cornerRadius = 11
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TwoOptionButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    @IBAction func didSelectButton(_ sender: AnyObject) {
        optionSelected.isHidden = false
        firstButton.isHidden = true
        secondButton.isHidden = true
        optionSelected.layer.cornerRadius = 11
        optionSelected.text = sender.titleLabel??.text
        responseReceived(option: sender.titleLabel??.text ?? "")
    }
    
    func responseReceived(option: String ) {
        UserDefaults.setUserSelectedOption(option)
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(notificationType.rawValue), object: nil)
    }
}

