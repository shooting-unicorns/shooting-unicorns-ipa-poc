var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var LodgedPatents = require('../models/LodgedPatents');
var PatentDescription = require('../models/PatentDescriptions')
var Trademarks = require('../models/Trademarks');
var AverageTimes = require('../models/AverageTimes')

router.get('/getPatentById/:patentAppNo', (req, res) => {
  var patentAppNo = req.params.patentAppNo;
  var patent = {}
  LodgedPatents.findPatentbyId(patentAppNo, (err, doc) =>{
    if (err) console.log("ERR: " + error);
    patent= JSON.parse(JSON.stringify(doc))
    PatentDescription.getPatentDescriptionByPatentId(patentAppNo, (err, descriptions) => {
      patent["description"] = descriptions["invention_description"];
      // Determine the status of the patent
      // Possible stages:
      // FILED => OPI => EXAMINATION REQUESTED => ACCEPTED => GRANTED
      // For testing: 2008237617 = FILED
      let patent_status = patent["patent_status_type"];
      let patent_type = patent["patent_type"];
      let null_date = "9999-12-31";

      if(patent_status == "FILED") {
        // Check at which stage of the "FILED" pipeline is falls under
        if (patent["opi_date"] == "" || patent["opi_date"] == null_date) {
          console.log("Awaiting OPI")
          patent["currentStatus"] = {
            status: "Awaiting OPI",
            averageDuration: AverageTimes[patent_type]["from_application_to_opi"]["average"]
          }

        } else if (!patent["exam_request_date"] == "" || !patent["exam_request_date"] == null_date) {
          let examination_status = patent["exam_status_type"];
          examination_status = examination_status ? examination_status : "Examination Requested"

          patent["currentStatus"] = {
            status: examination_status,
            averageDuration: AverageTimes[patent_type]["from_opi_examination_req"]["average"]
          }
          // patent["estimatedTimeToNextStage"] = AverageTimes[patent_type]["from_opi_examination_req"]["average"]
        }
      } else if (patent_status == "ACCEPTED") {
        console.log("ACCEPTED")
        patent["currentState"] = {
          status: "Accepted",
          average: AverageTimes[patent_type]['from_acceptance_to_sealing']
        }

      } else if (patent_status == "GRANTED") {
        console.log("GRANTED")
        patent["currentState"] = {
            status: "Granted/Sealed",
            average: ""
        }
      }

      res.send({result: patent});
    });
  });
});

router.get('/getTrademarkById/:trademarkId', (req, res) => {
  var trademarkId = req.params.trademarkId;

  Trademarks.findTradmarkById(trademarkId, (err, doc) => {
    if(err) console.log(err);
    res.send(doc);
  });
});

router.get('/getAllPatents', (req, res) => {
  PatentDescription.getAllPatents((err, doc) => {
    if (err) console.log("ERR: " + error);
    res.send(doc);
  });
});

module.exports = router;
