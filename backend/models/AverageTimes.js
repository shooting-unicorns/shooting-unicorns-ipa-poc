module.exports = {
  "Innovation": {
    "from_opi_examination_req": {
      "average": 6
    },
    "from_examination_acceptance": {
      "average": 0
    },
    "from_acceptance_to_sealing": {
      "average": 170
    },
    "from_application_to_opi": {
      "average": 10
    }
  },
  "Standard": {
    "from_opi_examination_req": {
      "average": 29
    },
    "from_examination_acceptance": {
      "average": 19
    },
    "from_acceptance_to_sealing": {
      "average": 3
    },
    "from_application_to_opi": {
      "average": 25
    }
  }
}
