const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');
// australian_appl_no	patent_type	requested_exam_type	exam_status_type	examination_section_name	acceptance_postponed_ind	patent_status_type	application_date	opi_date	expiry_date	exam_request_date	exam_request_filing_date	first_report_issue_date	further_report_issue_date	thrd_prty_exam_request_date	earliest_priority_date	acceptance_published_date	sealing_date	effective_patent_date	opi_published_in_journal_date	continue_renew_fee_paid_date
const LodgedPatentsSchema = new Schema({
  australian_appl_no: {type: String},
  patent_status_type: {type: String},
  invention_description: {type: String, required: true}
});

const LodgedPatents = mongoose.model('LodgedPatents', LodgedPatentsSchema, 'LodgedPatents');

exports.findPatentbyId = (patentNo, callback) => {
  console.log("Finding patent: " + patentNo);
  LodgedPatents.findOne({australian_appl_no: patentNo}, callback);
};

exports.getAllPatents = (callback) => {
  LodgedPatents.find({}).limit(20).exec(callback);
};
