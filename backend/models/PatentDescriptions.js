const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');

const PatentDescriptionSchema = new Schema({
  australian_appl_no: {type: String},
  invention_description: {type: String, required: true}
});

const PatentDescription = mongoose.model('PatentDescriptions', PatentDescriptionSchema, 'PatentDescriptions');

exports.getPatentDescriptionByPatentId = (patentNo, callback) => {
  console.log("Finding patent description: " + patentNo);
  PatentDescription.findOne({australian_appl_no: patentNo}, callback);
};

exports.getAllPatents = (callback) => {
  PatentDescription.find({}).limit(20).exec(callback);
};
