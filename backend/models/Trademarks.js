const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');

const TrademarksSchema = new Schema({
  tm_number: {type: String},
  trademark_text: {type: String},
  device_phrase_text: {type: String}
});

const PatentModel = mongoose.model('Trademarks', TrademarksSchema, 'Trademarks');

exports.findTradmarkById = (trademarkNo, callback) => {
  console.log("Finding trademark: " + trademarkNo);
  PatentModel.findOne({tm_number: trademarkNo}, callback);
};

exports.getAllTrademarks = (callback) => {
  PatentModel.find({}).limit(20).exec(callback);
};
