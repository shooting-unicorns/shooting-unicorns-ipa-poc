import csv
import json
from ftfy.badness import sequence_weirdness

csvfile = open('ipgod107-untouched.csv', 'r')
jsonfile = open('ipgod107-untouched.json', 'w')

reader = csv.DictReader( csvfile)
count = 0;
for row in reader:
    json.dump(row, jsonfile)
    jsonfile.write('\n')

    count += 1
print ("DONE")
