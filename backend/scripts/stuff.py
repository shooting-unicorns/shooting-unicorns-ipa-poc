from __future__ import division
import csv
import json
import numpy


csvfile = open('ipgod107-datediffed.csv', 'r')
jsonfile = open('average_times.json', 'w')
# from_application_to_opi	from_opi_examination_req	from_examination_acceptance	from_acceptance_to_sealing
# Standard and innovation
dict = {
    'Standard': {
        'from_application_to_opi': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_opi_examination_req': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_examination_acceptance': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_acceptance_to_sealing': {
            'total': 0,
            'count': 0,
            'average': 0
        }
    },
    'Innovation': {
        'from_application_to_opi': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_opi_examination_req': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_examination_acceptance': {
            'total': 0,
            'count': 0,
            'average': 0
        },
        'from_acceptance_to_sealing': {
            'total': 0,
            'count': 0,
            'average': 0
        }
    }
}
reader = csv.DictReader(csvfile)
count = 0;
for row in reader:
    patent_type = row['patent_type']
    from_application_to_opi = row['from_application_to_opi']
    from_opi_examination_req = row['from_opi_examination_req']
    from_examination_acceptance = row['from_examination_acceptance']
    from_acceptance_to_sealing = row['from_acceptance_to_sealing']

    if (from_application_to_opi and from_application_to_opi != '#NUM!'):
        dict[patent_type]['from_application_to_opi']['count'] += 1
        dict[patent_type]['from_application_to_opi']['total'] += int(from_application_to_opi)

        total = dict[patent_type]['from_application_to_opi']['total']
        count = dict[patent_type]['from_application_to_opi']['count']
        dict[patent_type]['from_application_to_opi']['average'] = int(round(total/count))

    if (from_opi_examination_req and from_examination_acceptance != '#NUM!'):
        dict[patent_type]['from_opi_examination_req']['count'] += 1
        dict[patent_type]['from_opi_examination_req']['total'] += int(from_opi_examination_req)

        total = dict[patent_type]['from_opi_examination_req']['total']
        count = dict[patent_type]['from_opi_examination_req']['count']
        dict[patent_type]['from_opi_examination_req']['average'] = int(round(total/count))

    if (from_examination_acceptance and from_examination_acceptance != '#NUM!'):
        dict[patent_type]['from_examination_acceptance']['count'] += 1
        dict[patent_type]['from_examination_acceptance']['total'] += int(from_examination_acceptance)

        total = dict[patent_type]['from_examination_acceptance']['total']
        count = dict[patent_type]['from_examination_acceptance']['count']
        dict[patent_type]['from_examination_acceptance']['average'] = int(round(total/count))

    if (from_acceptance_to_sealing and from_acceptance_to_sealing != '#NUM!'):
        dict[patent_type]['from_acceptance_to_sealing']['count'] += 1
        dict[patent_type]['from_acceptance_to_sealing']['total'] += int(from_acceptance_to_sealing)

        total = dict[patent_type]['from_acceptance_to_sealing']['total']
        count = dict[patent_type]['from_acceptance_to_sealing']['count']
        dict[patent_type]['from_acceptance_to_sealing']['average'] = int(round(total/count))

json.dump(dict, jsonfile)
print ("DONE")
